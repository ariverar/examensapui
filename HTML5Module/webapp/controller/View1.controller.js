
sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    
	'jquery.sap.global',
    'sap/ui/core/Fragment',
    'sap/ui/model/Filter',
    'sap/m/MessageBox',
  		

], function(  Controller, JSONModel,jQuery, Fragment,Filter,MessageBox) {
	"use strict";

	return Controller.extend("ns.HTML5Module.controller.View1", {
		onInit: function() {
            var oModel = new JSONModel("controller/Clothing.json");
            this.getView().setModel(oModel);
            this.byId("DP6").setMinDate(new Date());
            sap.m.DatePicker.prototype.onAfterRendering = function (e) {

            $('#'+e.srcControl.getId()+"-inner").prop('readonly', true);

            };
           },

            
           showJSONInfo: function(oEvent) {
            var oTable = this.getView().byId("TablaDetalle");
            var oModel2 = oTable.getModel();
            var aRows = oModel2.getData().catalog; 
            var cantidad = aRows.length;
            var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
            if (cantidad==0) {
                	MessageBox.error("Por favor ingresar data", {
				title: "Error",
				id: "messageBoxId2",
				styleClass: bCompact ? "sapUiSizeCompact" : "",
				contentWidth: "100px"
			});
            } else {
                sap.m.MessageBox.information("Esta seguro de enviar?", {
				title: "Information",
				id: "messageBoxId1",
				details: aRows,
				styleClass: bCompact ? "sapUiSizeCompact" : "",
				contentWidth: "100px"
            });
            }
			
            
		},
	    openQuickView: function (oEvent, oModel) {
			this.createPopover();
			this._oQuickView.close();
			this._oQuickView.setModel(oModel);
			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function () {
				this._oQuickView.openBy(oButton);
			});
		},
	    createPopover: function() {
			if (!this._oQuickView) {
				this._oQuickView = sap.ui.xmlfragment("ns.HTML5Module.view.Detalle", this);
			}
         	this.getView().addDependent(this._oQuickView);
		},
        detailsPress : function(oEvent) {
          var oTable = this.getView().byId("TablaDetalle");
            var oModel2 = oTable.getModel();
            var aRows = oModel2.getData().catalog; 
            var yy = aRows[oEvent.mParameters.rowIndex];
            
            /************************* */
            // create JSON model instance
			this.oModel = new JSONModel();
			// JSON sample data
			this.mData = {
				pages : [
					{
						pageId: "bankPage",
						header: "Detalle",
						title: "SAP Bank",
						icon: "sap-icon://building",
						description: "The bank of SAP",
						groups: [
							{
								heading: "Office",
								elements: [
									{
										label: "Doc Compras",
										value: yy.documento,
										elementType: sap.m.QuickViewGroupElementType.text
									},
									{
										label: "Cod. Detraccion",
										value: yy.codDetra,
										elementType: sap.m.QuickViewGroupElementType.text
                                    },
									{
										label: "Posicion",
										value: yy.posicion,
										elementType: sap.m.QuickViewGroupElementType.text
									},
									{
										label: "Impuesto",
										value: yy.impuesto,
										elementType: sap.m.QuickViewGroupElementType.text
									},
									{
										label: "Importe",
										value: yy.importe,
										elementType: sap.m.QuickViewGroupElementType.text
									},
									{
										label: "Moneda",
										value: yy.moneda,
										elementType: sap.m.QuickViewGroupElementType.text
									},
									{
										label: "fecha",
										value: yy.fecha,
										elementType: sap.m.QuickViewGroupElementType.text
									}
								]
							}
						]
					}

				]
			};
            this.oModel.setData(this.mData);
            this.openQuickView(oEvent, this.oModel);
        },
        _validateInput: function(oInput) {
			var oBinding = oInput.getValue("value");
            var sValueState = "None";

            var nameImput =  oInput.mProperties.name;

            if (nameImput=='importeTotal') {
                var bValidationError = false;
                var noSpaces = oBinding.replace(/ /g, "");  //Remove leading spaces
                var isNum = /^\d+$/.test(noSpaces); // test for numbers only and return true or false
                    if (!isNum) {
                        sValueState = "Error";
                        bValidationError = true;
                    }
                oInput.setValueState(sValueState);
                
            } else {
                var bValidationError = false;
                var noSpaces = oBinding.replace(/ /g, "");  //Remove leading spaces
                
                    if (noSpaces.length==0 ) {
                        sValueState = "Error";
                        bValidationError = true;
                    }
                oInput.setValueState(sValueState);
                
            }
            
            

			return bValidationError;
        },
        onDelete : function(){
            
            var oTable = this.getView().byId("TablaDetalle");
            var oModel2 = oTable.getModel();
            var aRows = oModel2.getData().catalog;
            
            if(aRows.length==0){
                MessageBox.warning("No existe data");
                return;
            }
            var aContexts = this.byId("TablaDetalle").getSelectedIndices();
           
            if(aContexts.length==0){
                MessageBox.warning("No eligio ningun item");
                return;
            }
           

            MessageBox.confirm("Esta seguro de Eliminar?", {
                title : "Informacion",
                initialFocus : sap.m.MessageBox.Action.CANCEL,
                
                onClose : function(sButton) {
                    if (sButton === MessageBox.Action.OK) {
                        for (var i = aContexts.length - 1; i >= 0; i--) {
                            aRows.splice(aContexts[i],1);
                        }
                         var oModel = this.getView().getModel();
                        var oData = oModel.oData;
                        oModel.setData(oData);
                        
                    } else{
                        return;
                    };
                }.bind(this)
            });

           




            
            



        },

        onContinue : function () {
			var that = this;
            var oView = this.getView();
            var aInputs = [
                oView.byId("productInput"),
                oView.byId("tipoOrden"),
                oView.byId("DP6"),
                oView.byId("importeTotal"),
                oView.byId("codDetra"),
                oView.byId("documentoUno"),
                oView.byId("documentoDos"),
                oView.byId("impuesto")
                            ];
			var bValidationError = false;
            jQuery.each(aInputs, function (i, oInput) {
                    bValidationError = that._validateInput(oInput) || bValidationError;
                });
			if (!bValidationError) {
               var productInput = oView.byId("productInput").getValue();
               var moneda = oView.byId("moneda").getValue();
               var tipoOrden = oView.byId("tipoOrden").getValue();
               var importeTotal = oView.byId("importeTotal").getValue();
               var codDetra = oView.byId("codDetra").getValue();
               var documentoUno = oView.byId("documentoUno").getValue();
               var documentoDos = oView.byId("documentoDos").getValue();
               var impuesto = oView.byId("impuesto").getValue();
               var DP6 = oView.byId("DP6").getValue() ;
               
               var parts =DP6.split('/');
               var dia =  parseInt(parts[0]);
              var mes =  parseInt(parts[1]);
              var anio =  parseInt('20'+parts[2]);
              var fecha =dia + "/" + mes  + "/" + anio;
              DP6=fecha;

                var oModel = this.getView().getModel();
                var backup = oModel.getData();
                var aEntries = oModel.getData().catalog;
                var mayor=0;
                var posiciones= new Array(Number);

                for (var i=0; i< aEntries.length; i++){
                       posiciones.push(aEntries[i].posicion); 
                       if(aEntries[i].posicion>=mayor){mayor=aEntries[i].posicion;}
                     }
                mayor = Number(mayor) +10;     
                var oEntry = {
                    "posicion":mayor,
                    "tipoDoc": productInput, 
                    "fecha": DP6,
                    "documento":documentoUno+documentoDos,
                    "moneda":moneda,
                    "importe":importeTotal,
                    "impuesto":impuesto,
                    "tipoOrden":tipoOrden,
                    "codDetra":codDetra};
                aEntries.unshift(oEntry);
                backup.catalog.sort(((a, b) => a.posicion - b.posicion));
               oModel.setData(backup);
               var productInput = oView.byId("productInput").setValue("");
               var moneda = oView.byId("moneda").setValue("");
               var tipoOrden = oView.byId("tipoOrden").setValue("");
               var importeTotal = oView.byId("importeTotal").setValue("");
               var codDetra = oView.byId("codDetra").setValue("");
               var documentoUno = oView.byId("documentoUno").setValue("");
               var documentoDos = oView.byId("documentoDos").setValue("");
               var impuesto = oView.byId("impuesto").setValue("");
               var DP6 = oView.byId("DP6").setValue("");

			} else {
               MessageBox.alert("Hubo un error en la validacion");
			}
		},
        handleValueHelp : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
            this.inputId = oEvent.getSource().getId();
            var nameImput = oEvent.oSource.mProperties.name ;
            if (!this._valueHelpDialog) {
                    this._valueHelpDialog = sap.ui.xmlfragment(
					"ns.HTML5Module.view.Dialog",
					this
                );
            this.getView().addDependent(this._valueHelpDialog);
            }
            this._valueHelpDialog.getBinding("items").filter([new Filter(
				"key",
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);
			this._valueHelpDialog.open(sInputValue);
        },

         handleValueHelp2 : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
            this.inputId = oEvent.getSource().getId();
           if (!this._valueHelpDialog2) {
                    this._valueHelpDialog2 = sap.ui.xmlfragment(
					"ns.HTML5Module.view.TipoOrden",
					this
                );
            this.getView().addDependent(this._valueHelpDialog2);
            }
            this._valueHelpDialog2.getBinding("items").filter([new Filter(
				"key",
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);
			this._valueHelpDialog2.open(sInputValue);
        },
         handleValueHelp3 : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
            this.inputId = oEvent.getSource().getId();
            var nameImput = oEvent.oSource.mProperties.name ;
           if (!this._valueHelpDialog3) {
                    this._valueHelpDialog3 = sap.ui.xmlfragment(
					"ns.HTML5Module.view.CodDetra",
					this
                );
            this.getView().addDependent(this._valueHelpDialog3);
            }
           this._valueHelpDialog3.getBinding("items").filter([new Filter(
				"key",
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);
			this._valueHelpDialog3.open(sInputValue);
        },
         handleValueHelp4 : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
            this.inputId = oEvent.getSource().getId();
            if (!this._valueHelpDialog4) {
                    this._valueHelpDialog4 = sap.ui.xmlfragment(
					"ns.HTML5Module.view.Impuesto",
					this
                );
            this.getView().addDependent(this._valueHelpDialog4);
            }
           	this._valueHelpDialog4.getBinding("items").filter([new Filter(
				"key",
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);
			this._valueHelpDialog4.open(sInputValue);
        },
        	_handleValueHelpSearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"value",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},
        _handleValueHelpClose : function (evt) {
            var oSelectedItem = evt.getParameter("selectedItem");
           if (oSelectedItem) {
				var productInput = this.byId(this.inputId);
				productInput.setValue(oSelectedItem.mProperties.description);
			}
			evt.getSource().getBinding("items").filter([]);
		}
	});
});